		;IFES	SERRA
		;Tabalho	de Arquitetura de Computadores - Alunos: Caio Mario e Renan
		;		Programa calcula 10 termos em progress�o geometrica de raz�o 2 a partir de X*100(600) onde X=6
		
		;Zerando	registradores que v�o ser utilizados
		AND		R4, R4, #0		; Pos. mem�ria
		AND		R5, R5, #0		; Valor a ser gravado
		AND		R6, R6, #0 		; Registrador RX do grupo
		AND		R7, R7, #0		; Contador AUX
		ADD		R4, R4, #200		; R4=R4+600 (Posi��o base na mem�ria 6*100)
		ADD		R4, R4, #200
		ADD		R4, R4, #200
		ADD		R6, R6, #32		; R5=R5+2 (Valor base de inicio da PG =32)
		STR		R6, [R4]		; Salva o valor base(2) na pos. 3000
		ADD		R7, R7, #1		; AUX=AUX+1
WHILE
		TST		R6, #2147483648	; Seta flags baseadas em R5 & (2^31), 2^31 � o valor m�ximo que a PG pode conter
		BNE		FINAL			; Se R5>2^31 pula para o fim
		LSL		R6, R6, #1		; Multiplica em 2^1 o valor (Deslocamento l�gico para a direita)
		ADD		R4, R4, #4		; R4=R4+4
		STR		R6, [R4]		; Guarda valor na posi��o R4
		ADD		R7, R7, #1		; AUX=AUX+1
		CMP		R7, #10		; Compara R6 com 9, flag zero restorna 1 se R6=9
		BEQ		FINAL			; Vai para END se R6=9 ou seja, faz o looping 9 vezes
		B		WHILE			; Volta para WHILE
FINAL
